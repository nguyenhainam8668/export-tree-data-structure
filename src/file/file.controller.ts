import { Controller, Get, StreamableFile } from '@nestjs/common';
import { createReadStream } from 'fs';
import { join } from 'path';
import { FileService } from './file.service';

@Controller('file')
export class FileController {
    constructor (
        private fileService: FileService
    ) {}

    @Get()
    async getFile() {
        await this.fileService.getFile()

        const file = createReadStream(join(process.cwd(), 'src', 'test.xlsx'))

        return new StreamableFile(file)
    }
}
