import { Injectable } from '@nestjs/common';
import { ExportTreeStructuredData } from 'src/utils/getData';

@Injectable()
export class FileService {

    async getFile() {
        const exportTree = new ExportTreeStructuredData()
        await exportTree.exportData()
    }

}
