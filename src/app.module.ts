import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MembersModule } from './members/members.module';
import { GroupsModule } from './groups/groups.module';
import { JobModule } from './job/job.module';
import { SequelizeModule } from '@nestjs/sequelize';

import { Member } from './members/entities/member.entity';
import { Job } from './job/entities/job.entity';
import { Group } from './groups/entities/group.entity';
import { FileModule } from './file/file.module';

@Module({
  imports: [
    MembersModule,
    GroupsModule,
    JobModule,  
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'congkepostgres',
      database: 'mydb',
      models: [Member, Job, Group],
      synchronize: true,
      sync: {
        alter: true
      },
      autoLoadModels: true
    }), FileModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
