import { Module } from '@nestjs/common';
import { JobService } from './job.service';
import { JobController } from './job.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Job } from './entities/job.entity';
import { MembersModule } from 'src/members/members.module';

@Module({
  imports: [SequelizeModule.forFeature([Job]), MembersModule],
  controllers: [JobController],
  providers: [JobService],
})
export class JobModule {}
