import { Model, Table, Column, DataType, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Member } from "src/members/entities/member.entity";

@Table({
    timestamps: false
})
export class Job extends Model {
    id: number;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true
    })
    name: string;

    @ForeignKey(() => Member)
    @Column({
        type: DataType.INTEGER,
    })
    memberId: number;

    @BelongsTo(() => Member)
    member: Member;

    @Column({
        type: DataType.INTEGER
    })
    score: number
}
