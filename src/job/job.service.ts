import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Job } from './entities/job.entity';
import { MembersService } from 'src/members/members.service';
import { Member } from 'src/members/entities/member.entity';

@Injectable()
export class JobService {
  constructor(
    @InjectModel(Job)
    private jobModel: typeof Job,
    private membersService: MembersService,
  ) {}

  async create(createJobDto: CreateJobDto) {
    const { name, memberId, score } = createJobDto;

    /// Check if name existed
    const job = await this.jobModel.findOne({ where: { name } });

    if (job) {
      throw new BadRequestException('Job name already existed');
    }

    /// Find if group exists
    const member = await this.membersService.findOne(memberId);

    /// Member not existed
    if (!member) {
      throw new NotFoundException('Member not existed');
    }

    console.log(`Member with memberId ${memberId} `, member);

    const newJob = await this.jobModel.create({ name, memberId, score });
    return newJob;
  }

  async findAll() {
    return await this.jobModel.findAll();
  }

  async findOne(id: number) {
    const job = await this.jobModel.findOne({
      where: {
        id,
      },
      include: Member,
    });

    if (!job)
      throw new BadRequestException("Job not found")

    return job
  }

  async update(id: number, updateJobDto: UpdateJobDto) {
    const job = await this.findOne(id);

    if (!job) throw new BadRequestException('Job not found');

    return await this.jobModel.update(
      { ...updateJobDto },
      { where: { id }, returning: true },
    );
  }

  async remove(id: number) {
    const job = await this.findOne(id);

    if (!job) throw new BadRequestException('Job not found');

    return await this.jobModel.destroy({ where: { id } });
  }
}
