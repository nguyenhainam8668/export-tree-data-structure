import {
  BadRequestException,
  Injectable,
  NotFoundException,
  StreamableFile,
} from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Group } from './entities/group.entity';
import { Member } from 'src/members/entities/member.entity';

import { Job } from 'src/job/entities/job.entity';
import * as ExcelJS from 'exceljs';

/// Group map type
type GroupMap = {
  [key: number]: number[];
};

type GroupData = {
  order: string;
  name: string;
  totalMembers: number;
  totalScore: number;
  jobs: string
};

type GroupCountMembers = {
  [key: number]: number;
};

type ScoreAndJob = {
  score: number;
  jobs: string[];
};
type GroupScore = {
  // [key: number]: number;
  [key: number]: ScoreAndJob;
};

type GroupIdMap = {
  [key: number]: Group;
};

type GroupsList = GroupData[];

@Injectable()
export class GroupsService {
  constructor(
    @InjectModel(Group)
    private groupModel: typeof Group,
  ) {}

  async create(createGroupDto: CreateGroupDto) {
    const { name, parentId } = createGroupDto;

    /// Check if name existed
    const existedGroup = await this.groupModel.findOne({ where: { name } });

    if (existedGroup) {
      throw new BadRequestException('Group name already existed');
    }

    let newGroup;
    const allCurrentGroups = await this.groupModel.findAll();

    /// Currently no groups
    if (allCurrentGroups.length == 0) {
      console.log('All current groups', allCurrentGroups);

      /// if no group and parentId not null
      if (parentId) {
        throw new BadRequestException('Root group cannot have a parent group');
      }

      newGroup = await Group.create({ name, parentId });
      return newGroup;
    } else {
      /// If there are groups but request does not have parentId
      if (!parentId) {
        throw new BadRequestException('Parent group id is required');
      }

      /// Check if parent group existed
      const parentExisted = allCurrentGroups.some(
        (group, _) => group.id == +parentId,
      );

      /// Not existed, throw bad request
      if (!parentExisted)
        throw new BadRequestException('Parent group does not exist');

      newGroup = await Group.create({ ...createGroupDto });
      return newGroup;
    }
  }

  async findAll() {
    return await this.groupModel.findAll();
  }

  async findOne(id: number) {
    const group = await this.groupModel.findOne({
      where: {
        id,
      },
      include: Member,
    });

    if (!group) throw new NotFoundException('Group not found');

    return group;
  }

  async update(id: number, updateGroupDto: UpdateGroupDto) {
    const group = await this.groupModel.findOne({
      where: {
        id,
      },
    });

    if (!group) throw new NotFoundException('Group not found');
    return this.groupModel.update(
      { ...updateGroupDto },
      { where: { id }, returning: true },
    );
  }

  async remove(id: number) {
    const group = await this.groupModel.findOne({
      where: {
        id,
      },
    });

    if (!group) throw new NotFoundException('Group not found');
    return await this.groupModel.destroy({ where: { id } });
  }

  async exportData() {
    let groupsList: GroupsList = [];
    let groupCountMembers: GroupCountMembers = {};
    let groupScore: GroupScore = {};
    let groupIdMap: GroupIdMap = {};

    await this.getAllData(
      groupsList,
      groupCountMembers,
      groupScore,
      groupIdMap,
    );

    /// Creating a workbook
    const workbook = new ExcelJS.Workbook();

    /// Add worksheet
    const ws = workbook.addWorksheet('Groups');

    /// Creating columns
    ws.columns = [
      { header: 'Group order', key: 'order', width: 20},
      { header: 'Name', key: 'name', width: 20},
      { header: 'Number of members', key: 'totalMembers', width: 20},
      { header: 'Total Score', key: 'totalScore', width: 20},
      { header: 'Jobs', key: 'jobs', width: 50}
    ];

    /// Format       
    ws.getColumn(1).alignment = { vertical: 'top', horizontal: 'left'}
    ws.getColumn(2).alignment = { vertical: 'top', horizontal: 'left'}
    ws.getColumn(3).alignment = { vertical: 'top', horizontal: 'left'}
    ws.getColumn(4).alignment = { vertical: 'top', horizontal: 'left'}
    ws.getColumn(5).alignment = { wrapText: true, horizontal: 'left'}

    /// Adding rows
    ws.addRows(groupsList);
    const filename = `${process.cwd()}/src/test.xlsx`;

    await workbook.xlsx.writeFile(filename);

    const buffer = (await workbook.xlsx.writeBuffer()) as Buffer;

    return new StreamableFile(buffer);
  }

  async getAllData(groupsList, groupCountMembers, groupScore, groupIdMap) {
    const groups = await this.groupModel.findAll();

    // console.log("All groups", groups)
    let groupMap: GroupMap = {};

    groups.forEach((group) => {
      console.log(group.dataValues);

      if (!groupMap[group.parentId]) groupMap[group.parentId] = [];

      groupMap[group.parentId].push(group.dataValues.id);
    });

    for (const group of groups) {
      /// Count members
      await this.getCountMembers(group.id, groupMap, groupCountMembers);
      // console.log('Group and count member ', group.id, cnt);

      /// Count score
      await this.getScore(group.id, groupMap, groupScore);
      // console.log('Group and score ', group.id, score);

      /// Name
      groupIdMap[group.id] = group.dataValues;
    }

    console.log('Group Map', groupScore, groupCountMembers, groupIdMap);

    await this.dfsToGetData(
      groupMap,
      groupCountMembers,
      groupScore,
      groupIdMap,
      groupsList,
    );

    console.log(groupsList);
  }

  async getCountMembers(
    groupId: number,
    groupMap: GroupMap,
    groupCountMembers: GroupCountMembers,
  ) {
    if (groupCountMembers[groupId]) return groupCountMembers[groupId];

    const group = await this.groupModel.findOne({
      where: { id: groupId },
      include: Member,
    });

    // console.log(group.dataValues.members.length)
    let count = group.dataValues.members.length;

    const childrenGroups = groupMap[groupId];

    if (!childrenGroups) {
      groupCountMembers[groupId] = count;
      return count;
    }
    for (const childGroup of childrenGroups) {
      const tmp = await this.getCountMembers(
        childGroup,
        groupMap,
        groupCountMembers,
      );
      count += tmp;
    }

    groupCountMembers[groupId] = count;
    return count;
  }

  async getScore(
    groupId: number,
    groupMap: GroupMap,
    groupScore: GroupScore,
  ): Promise<ScoreAndJob> {
    if (groupScore[groupId]) return groupScore[groupId];

    const group = await this.groupModel.findOne({
      where: { id: groupId },
      include: Member,
    });

    let score = 0,
      jobs = [];

    /// Find all members of this group and add up score
    for (const member of group.dataValues.members) {
      const tmp = await Member.findOne({
        where: { id: member.id },
        include: Job,
      });

      tmp.dataValues.jobs.forEach((job: any) => (score += job.score));
      jobs.push(...tmp.dataValues.jobs.map((job) => job.name));
    }

    // console.log("Group and score", groupId, score)

    const childrenGroups = groupMap[groupId];

    if (!childrenGroups) {
      groupScore[groupId] = { score, jobs };
      return { score, jobs };
    }

    for (const childGroup of childrenGroups) {
      const tmpScoreJob = await this.getScore(childGroup, groupMap, groupScore);
      score += tmpScoreJob.score;
      jobs.push(...tmpScoreJob.jobs);
    }

    groupScore[groupId] = { score, jobs };
    return { score, jobs };
  }

  async dfsToGetData(
    groupMap: GroupMap,
    groupCountMembers: GroupCountMembers,
    groupScore: GroupScore,
    groupIdMap: GroupIdMap,
    groupsList: GroupsList,
  ) {
    const rootId = groupMap[null as string];

    const dfs = async (rootId: number, order: string) => {
      const thisGroupData: GroupData = {
        order,
        name: groupIdMap[rootId].name,
        totalMembers: groupCountMembers[rootId],
        totalScore: groupScore[rootId].score,
        jobs: groupScore[rootId].jobs.join()
      };

      groupsList.push(thisGroupData);

      const childrenGroups = groupMap[rootId];

      if (!childrenGroups) {
        return;
      }

      let idx = 1;
      for (const childGroupId of childrenGroups) {
        await dfs(childGroupId, `${order}.${idx}`);
        idx++;
      }
    };

    await dfs(rootId, '1');
    // console.log(this.groupsList)
  }
}
