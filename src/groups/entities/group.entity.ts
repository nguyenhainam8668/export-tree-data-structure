import {
  Model,
  Table,
  Column,
  DataType,
  PrimaryKey,
  AutoIncrement,
  HasMany,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Member } from 'src/members/entities/member.entity';

@Table({
  timestamps: false,
})
export class Group extends Model {
  id: number;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: true,
  })
  name: string;

  // @ForeignKey(() => Group)
  @Column({
    type: DataType.STRING,
  })
  parentId: string;

  // @BelongsTo(() => Group)
  parent: Group;

  // @HasMany(() => Group)
  // childGroups: Group[];

  @HasMany(() => Member)
  members: Member[]
}
