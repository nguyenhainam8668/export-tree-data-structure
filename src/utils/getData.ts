import { CreateGroupDto } from 'src/groups/dto/create-group.dto';
import { Group } from 'src/groups/entities/group.entity';
import { Job } from 'src/job/entities/job.entity';
import { Member } from 'src/members/entities/member.entity';
import * as ExcelJS from 'exceljs'

/// Group map type
type GroupMap = {
  [key: number]: number[];
};

type GroupData = {
  order: string;
  name: string;
  totalMembers: number;
  totalScore: number;
};

type GroupCountMembers = {
  [key: number]: number;
};

type GroupScore = {
  [key: number]: number;
};

type GroupIdMap = {
  [key: number]: Group;
};

type GroupsList = GroupData[];

export class ExportTreeStructuredData {
  constructor(
    private groupsList: GroupsList = [],
    private groupCountMembers: GroupCountMembers = {},
    private groupScore: GroupScore = {},
    private groupIdMap: GroupIdMap = {},
  ) {}

  async exportData() {
    await this.getAllData()

    /// Creating a workbook
    const workbook = new ExcelJS.Workbook()

    /// Add worksheet
    const ws = workbook.addWorksheet('Groups')


    /// Creating columns
    ws.columns = [
      { header: 'Group order', key: 'order'},
      { header: 'Name', key: 'name'},
      { header: 'Number of members', key: 'totalMembers'},
      { header: 'Total Score', key: 'totalScore'}
    ]

    /// Adding rows
    ws.addRows(this.groupsList)
    const filename = `${process.cwd()}/src/test.xlsx`

    await workbook.xlsx.writeFile(filename)
  }

  async getAllData() {
    const groups = await Group.findAll();

    // console.log("All groups", groups)
    let groupMap: GroupMap = {};

    groups.forEach((group) => {
      console.log(group.dataValues);

      if (!groupMap[group.parentId]) groupMap[group.parentId] = [];

      groupMap[group.parentId].push(group.dataValues.id);
    });

    for (const group of groups) {
      /// Count members
      await this.getCountMembers(group.id, groupMap);
      // console.log('Group and count member ', group.id, cnt);

      /// Count score
      await this.getScore(group.id, groupMap);
      // console.log('Group and score ', group.id, score);

      /// Name
      this.groupIdMap[group.id] = group.dataValues;
    }

    console.log(
      'Group Map',
      this.groupScore,
      this.groupCountMembers,
      this.groupIdMap,
    );
    
    await this.dfsToGetData(groupMap);

    console.log(this.groupsList)
  }

  async getCountMembers(groupId: number, groupMap: GroupMap) {
    if (this.groupCountMembers[groupId]) return this.groupCountMembers[groupId];

    const group = await Group.findOne({
      where: { id: groupId },
      include: Member,
    });

    // console.log(group.dataValues.members.length)
    let count = group.dataValues.members.length;

    const childrenGroups = groupMap[groupId];

    if (!childrenGroups) {
      this.groupCountMembers[groupId] = count;
      return count;
    }
    for (const childGroup of childrenGroups) {
      const tmp = await this.getCountMembers(childGroup, groupMap);
      count += tmp;
    }

    this.groupCountMembers[groupId] = count;
    return count;
  }

  async getScore(groupId: number, groupMap: GroupMap) {
    if (this.groupScore[groupId]) return this.groupScore[groupId];

    const group = await Group.findOne({
      where: { id: groupId },
      include: Member,
    });

    let score = 0;

    /// Find all members of this group and add up score
    for (const member of group.dataValues.members) {
      const tmp = await Member.findOne({
        where: { id: member.id },
        include: Job,
      });

      tmp.dataValues.jobs.forEach((job: any) => (score += job.score));
    }

    // console.log("Group and score", groupId, score)

    const childrenGroups = groupMap[groupId];

    if (!childrenGroups) {
      this.groupScore[groupId] = score;
      return score;
    }

    for (const childGroup of childrenGroups) {
      const tmp = await this.getScore(childGroup, groupMap);
      score += tmp;
    }

    this.groupScore[groupId] = score;
    return score;
  }

  async dfsToGetData(groupMap: GroupMap) {
    const rootId = groupMap[null as string];

    const dfs = async (rootId: number, order: string) => {
      const thisGroupData: GroupData = {
        order,
        name: this.groupIdMap[rootId].name,
        totalMembers: this.groupCountMembers[rootId],
        totalScore: this.groupScore[rootId],
      };

      this.groupsList.push(thisGroupData);

      const childrenGroups = groupMap[rootId];

      if (!childrenGroups) {
        return 
      }

      let idx = 1
      for (const childGroupId of childrenGroups)
      { 
        await dfs(childGroupId, `${order}.${idx}`)
        idx++
      }
    };

    await dfs(rootId, "1")
    // console.log(this.groupsList)
  }
}
