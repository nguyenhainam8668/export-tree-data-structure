import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateMemberDto {
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsNumber()
    @IsNotEmpty()
    groupId: number;
}
