import { Module } from '@nestjs/common';
import { MembersService } from './members.service';
import { MembersController } from './members.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Member } from './entities/member.entity';
import { GroupsModule } from 'src/groups/groups.module';

@Module({
  imports: [SequelizeModule.forFeature([Member]), GroupsModule],
  controllers: [MembersController],
  providers: [MembersService],
  exports: [MembersService]
})
export class MembersModule {}
