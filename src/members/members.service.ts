import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { InjectModel } from '@nestjs/sequelize';
import { Member } from './entities/member.entity';
import { GroupsService } from 'src/groups/groups.service';
import { Group } from 'src/groups/entities/group.entity';
import { Job } from 'src/job/entities/job.entity';
import { where } from 'sequelize';

@Injectable()
export class MembersService {
  constructor(
    @InjectModel(Member)
    private memberModel: typeof Member,
    private groupsService: GroupsService,
  ) {}

  async create(createMemberDto: CreateMemberDto) {
    const { name, groupId } = createMemberDto;

    /// Find if group exists
    const group = await this.groupsService.findOne(groupId);

    /// Group not existed
    if (!group) {
      throw new NotFoundException('Group not existed');
    }

    console.log(`Group with groupId ${groupId} `, group);

    const member = await this.memberModel.findOne({ where: { name } })

    if (member)
      throw new BadRequestException("This member is already existed in database")

    const newMember = await this.memberModel.create({ name, groupId });
    return newMember;
  }

  async findAll() {
    return this.memberModel.findAll({
      attributes: ['id', 'name', 'groupId'],
    });
  }

  async findOne(id: number) {
    const member = await this.memberModel.findOne({
      where: {
        id,
      },
      include: [Group, Job],
    });

    // console.log(`Member with id ${id}`, member.dataValues)

    if (!member) throw new NotFoundException('Member not found');

    return member;
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const member = await this.memberModel.findOne({
      where: {
        id,
      },
      include: Group,
    });

    // console.log(`Member with id ${id}`, member.dataValues)

    if (!member) throw new NotFoundException('Member not found');
    return this.memberModel.update(
      { ...updateMemberDto },
      { where: { id }, returning: true },
    );
  }

  async remove(id: number) {
    const member = await this.memberModel.findOne({
      where: {
        id,
      },
      include: Group,
    });

    // console.log(`Member with id ${id}`, member.dataValues)

    if (!member) throw new NotFoundException('Member not found');
    return this.memberModel.destroy({ where: { id } });
  }
}
