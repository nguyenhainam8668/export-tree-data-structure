import { BelongsTo, Column, DataType, ForeignKey, HasMany, Model, Table } from "sequelize-typescript";
import { Group } from "src/groups/entities/group.entity";
import { Job } from "src/job/entities/job.entity";

@Table({
    tableName: 'Members',
    timestamps: false
})
export class Member extends Model {
    id: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true
    })
    name: string; 

    @ForeignKey(() => Group)
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
    })
    groupId: number;

    @BelongsTo(() => Group)
    group: Group;

    @HasMany(() => Job)
    jobs: Job[]
}
