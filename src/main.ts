import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExportTreeStructuredData } from './utils/getData';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(8080);

  // const exportTree = new ExportTreeStructuredData()
  // exportTree.exportData()
}
bootstrap();
